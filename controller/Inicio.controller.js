sap.ui.define([
	"sap/ui/core/mvc/Controller",
	'sap/ui/model/Filter',
	'sap/ui/model/json/JSONModel',
	'sap/m/TableSelectDialog',
	'sap/m/Column',
	'sap/m/ColumnListItem'
], function(Controller) {
	"use strict";

	return Controller.extend("basic.controller.Inicio", {
		onInit: function(oEvent){
			var dataObject = [{
            Product: "Power Projector 4713",
            Weight: "1467"
			}, {
				Product: "Gladiator MX",
				Weight: "321"
			}, {
				Product: "Hurricane GX",
				Weight: "588"
			}, {
				Product: "Webcam",
				Weight: "700"
			}, {
				Product: "Monitor Locking Cable",
				Weight: "40"
			}, {
				Product: "Laptop Case",
				Weight: "1289"
			}];
			$.each(dataObject, function(i, value) {
			dataObject[i].selected = true;
			});
			var oModel = new sap.ui.model.json.JSONModel();
			oModel.setData(dataObject);
			sap.ui.getCore().setModel(oModel);
		}, 
		onPress: function(oEvent) {
          	var oTableSelectDialog = new sap.m.TableSelectDialog({
	            title: "Products",
	            multiSelect: true,
	            rememberSelections: true,
	            columns: [
		            new sap.m.Column({
		                header: new sap.m.Label({
	                  		text: "Product"
		                })
		          	}),
		          	new sap.m.Column({
		                header: new sap.m.Label({
	                  		text: "Weight"
		                })
		          	})
	            ],
	            items: {
		          	path: '/',
		          	template: new sap.m.ColumnListItem({
		                selected: "{selected}",
		                cells: [
			                new sap.m.Text({
		                    	text: "{Product}"
		                  	}),
		                  	new sap.m.Text({
		                	    text: "{Weight}"
		            	  	})
		                ]
	          		})
	            },
	            confirm: function(oEvent) {
	          		var oItems = oEvent.getParameter("selectedItems");
	            }
      		});
      		oTableSelectDialog.open();
        }
	});
});