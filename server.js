const express = require('express');
var bodyParser = require('body-parser');
const Sequelize = require('sequelize');
//var $ = require('jQuery');
var fs = require('fs');
var util = require("util");

const app = express();

app.use( bodyParser.json() );       // to support JSON-encoded bodies

app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
}));

// const sdkDir = __dirname + '/openui5-sdk-1.48.8'
const sdkDir = '../openui5-sdk-1.42.7'

const escenarioDeCobro = '../basic'

const port = 8800
const launchUrl = `http://localhost:${port}/`

app.use('/sdk', express.static(sdkDir));
// app.use('/sdk2', express.static(sdkDir));


app.use('/app1', express.static(escenarioDeCobro));

app.listen(port);

console.log("UI5 app running at\n  => " + launchUrl + " \nCTRL + C to shutdown")